package demo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.sbgn.SbgnUtil;
import org.sbgn.bindings.*;

import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Simple integration test for reading SBGN PD files in a Maven project.
 *
 * Based on
 * <a href='https://github.com/cannin/libsbgn/blob/master/org.sbgn/examples/org/sbgn/ReadExample.java'>ReadExample.java</a>
 *
 * @author Mihai Glon\u021b mglont@ebi.ac.uk
 */
@RunWith(JUnit4.class)
public class IntegrationTest {

    @Test
    public void shouldPrintArcs() throws JAXBException {
        // https://github.com/cannin/libsbgn/blob/b8094d3/test-files/PD/adh.sbgn
        InputStream is = getClass().getResourceAsStream("/adh.sbgn");

        Sbgn sbgn = SbgnUtil.readFrom(is);

        // map is a container for the glyphs and arcs
        Map map = sbgn.getMap().get(0);

        List<Glyph> glyphs = map.getGlyph();
        List<Glyph> nadGlyphs = glyphs.stream()
                .filter(g -> Optional.ofNullable(g.getLabel())
                        .filter(l -> l.getText().startsWith("NAD"))
                        .isPresent())
                .collect(Collectors.toList());
        Assert.assertEquals(2, nadGlyphs.size());
        Assert.assertTrue(nadGlyphs.stream()
                .map(Glyph::getLabel)
                .map(Label::getText)
                .collect(Collectors.toList())
                .containsAll(Arrays.asList("NAD+", "NADH")));
        Assert.assertEquals("glyph_nad", nadGlyphs.get(0).getId());
        Assert.assertEquals("simple chemical", nadGlyphs.get(0).getClazz());
        Assert.assertNull(glyphs.get(glyphs.size() - 1).getLabel());
    }
}
